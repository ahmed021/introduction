import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('About', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('Asma Ahmed', style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),
          Icon(Icons.home, color: Colors.red[500],),
          Text('11'),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.local_airport, 'Travel'),
          _buildButtonColumn(color, Icons.laptop_mac, 'CS'),
          _buildButtonColumn(color, Icons.mail_outline, 'Contact'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Hi. My name is Asma Ahmed and I am a 4th year computer science major at California'
            'State University, San Marcos. I hope to graduate this Spring 2021. Some interesting'
            'facts about myself are I have a twin sister and 4 older siblings! I am the vice president'
            'of the Developers Student Club(DSC) on campus, and my spirit animal is a horse.',
        softWrap: true,
      ),
    );
    return MaterialApp(
      title: 'Homework 2 - Layout',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Homework 2 - Layout'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/Profile_pic.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSection,
            textSection
          ]
        ),
      ),
    );
  }
  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
} //MyApp